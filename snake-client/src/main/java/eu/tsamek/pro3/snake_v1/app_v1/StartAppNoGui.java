package eu.tsamek.pro3.snake_v1.app_v1;

import eu.tsamek.pro3.snake_v1.core_v1.impl.SnakeRunner;

/**
 * T��da staraj�c� se o hladk� b�h hada v negrafick�m rozhran�
 *
 * @author Tom� Samek
 * @version 1.0
 * @created 24-VIII-2007 18:30:07
 */
public class StartAppNoGui {

  /**
   * @param args 1 - host, 2 - port, 3 - AI
   */
  public static void main(String[] args) {

    System.out.println("Running snake in silent AI remote mode ...");
    if (args.length == 3 || args.length == 2) {
      String[] s = new String[3];
      s[0] = args[0];
      s[1] = args[1];
      s[2] = "true";
      SnakeRunner.init(s, true);
    } else {
      SnakeRunner.init(args, true);
    }


  }

}
