package eu.tsamek.pro3.snake_v1.app_v1;

import eu.tsamek.pro3.snake_v1.core_v1.impl.SnakeRunner;

public class StartApp {

  /**
   * @param args 1 - host, 2 - port, 3 - AI
   */
  public static void main(String[] args) {
    String[] arguments = new String[3];
    arguments[0] = "127.0.0.1";
    arguments[1] = "6666";
    arguments[2] = "true";

    SnakeRunner.init(arguments, false);

  }

}
