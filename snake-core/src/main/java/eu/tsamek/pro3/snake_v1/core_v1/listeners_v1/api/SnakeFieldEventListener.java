package eu.tsamek.pro3.snake_v1.core_v1.listeners_v1.api;

import eu.tsamek.pro3.snake_v1.core_v1.events_v1.impl.SnakeCollisionEvent;
import eu.tsamek.pro3.snake_v1.core_v1.events_v1.impl.SnakeFieldEvent;

import java.util.EventListener;

/**
 * @author Tom� Samek
 */
public interface SnakeFieldEventListener extends EventListener {

  /**
   * ud�lost kdy dojde k p�id�n� pol��ka do pole
   *
   * @param evt SnakeFieldEvent
   */
  void snakeFieldAdded(SnakeFieldEvent evt);

  /**
   * ud�lost kdy dojde k odebr�n� pol��ka z pole
   *
   * @param evt SnakeFieldEvent
   */
  void snakeFieldRemoved(SnakeFieldEvent evt);

  /**
   * ud�lost kdy dojde ke kolizi na plo�e
   *
   * @param evt SnakeCollisionEvent
   */
  void snakeFieldCollision(SnakeCollisionEvent evt);

  /**
   * ud�lost kdy se zm�n� data na gridu
   */
  void snakeDataChanged();

}
