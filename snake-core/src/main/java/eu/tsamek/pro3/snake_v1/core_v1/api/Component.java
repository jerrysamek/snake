package eu.tsamek.pro3.snake_v1.core_v1.api;

import java.io.Serializable;

/**
 * @author Tom� Samek
 * <p>
 * Defaultn� komponenta kter� umo��uje vytvo�en� komponent,
 * kter� z n� d�d� p�es t��du ComponentFactory
 * @version 1.0
 * @created 24-VIII-2007 18:30:05
 */
public interface Component extends Serializable {


}