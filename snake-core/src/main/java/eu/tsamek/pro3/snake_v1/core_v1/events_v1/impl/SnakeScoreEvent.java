package eu.tsamek.pro3.snake_v1.core_v1.events_v1.impl;

import java.util.EventObject;
import java.util.Map;

/**
 * @author Tom� Samek
 * Ud�lost vyvolan� zm�nou sk�re
 */
public class SnakeScoreEvent extends EventObject {

  private static final long serialVersionUID = -6209384058442378349L;
  private int score;
  private Map<String, String> scoreMap;

  public SnakeScoreEvent(Object source, Map<String, String> scoreMap) {
    super(source);
    this.scoreMap = scoreMap;
  }

  public SnakeScoreEvent(Object source, int score) {
    super(source);
    this.score = score;
  }

  /**
   * @return aktu�ln� sk�re
   */
  public int getScore() {
    return score;
  }

  /**
   * @return vrac� seznam v�ech had� a jejich jednotliv�ch sk�re
   */
  public Map<String, String> getScoreMap() {
    return scoreMap;
  }

}
