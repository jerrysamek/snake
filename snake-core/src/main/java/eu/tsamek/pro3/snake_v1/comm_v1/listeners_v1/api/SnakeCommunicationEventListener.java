package eu.tsamek.pro3.snake_v1.comm_v1.listeners_v1.api;

import eu.tsamek.pro3.snake_v1.comm_v1.events_1.impl.SnakeCommunicationEvent;

import java.util.EventListener;

/**
 * @author Tom� Samek
 * Interface definuj�c� listener pro klienta
 */
public interface SnakeCommunicationEventListener extends EventListener {

  /**
   * Ud�lost vyvolan� p�i p�ijet� dat
   *
   * @param SnakeCommunicationEvent
   */
  void dataReceived(SnakeCommunicationEvent evt);

  /**
   * Ud�lost vyvolan� odpojen�m serveru
   */
  void serverDisconnected();

  /**
   * Ud�lost vyvolan� pozitivn�m p�ipojen�m k serveru
   *
   * @param SnakeCommunicationEvent
   */
  void serverConnected(SnakeCommunicationEvent evt);
}
