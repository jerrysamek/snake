package eu.tsamek.pro3.snake_v1.gui_v1.render_v1.api;

import java.awt.*;

/**
 * @author Tom� Samek
 * Ur�uje jak by m�l vypadat render pro pol��ko
 */
public interface SnakeRender {

  /**
   * vykresl� pole na zadan� m�sto na plo�e
   *
   * @param g        Graphics
   * @param position pozice v poli
   */
  void renderPart(Graphics g, Point position);
}
