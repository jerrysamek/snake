package eu.tsamek.pro3.snake_v1.core_v1.snake_v1.impl;

import eu.tsamek.pro3.snake_v1.core_v1.api.MoveManager;
import eu.tsamek.pro3.snake_v1.core_v1.api.Snake;
import eu.tsamek.pro3.snake_v1.core_v1.events_v1.impl.SnakeMoveEvent;
import eu.tsamek.pro3.snake_v1.core_v1.events_v1.impl.SnakeWasCreatedEvent;
import eu.tsamek.pro3.snake_v1.core_v1.impl.AbstractComponent;
import eu.tsamek.pro3.snake_v1.core_v1.impl.ComponentFactory;
import eu.tsamek.pro3.snake_v1.core_v1.listeners_v1.api.SnakeMoveEventListener;
import eu.tsamek.pro3.snake_v1.core_v1.listeners_v1.api.SnakeWasCreatedEventListener;

import java.awt.*;
import java.util.Map;
import java.util.Vector;

/**
 * @author Tom� Samek
 * @version 1.0
 * @created 24-VIII-2007 18:30:07
 */
public class SnakeImpl extends AbstractComponent implements Snake {

  private static final long serialVersionUID = 5022155435991931484L;
  protected javax.swing.event.EventListenerList listenerList =
      new javax.swing.event.EventListenerList();
  int bodySize;
  private Vector<Point> snake = new Vector<Point>();
  private MoveManager moveManager;

  /**
   * @param attributes mapa atribut� - A_HEAD_POS(Point pozice hlavy), A_BODY_SIZE(int d�lka t�la) a A_SNAKE_DIRECTION (String sm�r pohybu)
   */
  public SnakeImpl(Map<String, Object> attributes) {
    super(attributes);

    Point tail = (Point) attributes.get(A_HEAD_POS);
    bodySize = (Integer) attributes.get(A_BODY_SIZE);
    String direction = (String) attributes.get(A_SNAKE_DIRECTION);

    addSnakeWasCreatedListener((SnakeWasCreatedEventListener) attributes.get(A_CREATED_LISTENER));

    moveManager = (MoveManager) ComponentFactory.createComponent(ComponentFactory.COM_MOVEMANAGER);
    moveManager.changeDirection(direction);

    for (int i = 0; i < bodySize; i++) {
      snake.add(tail);
      tail = moveManager.moveBack(tail);
    }

    fireSnakeCreated(new SnakeWasCreatedEvent(this, snake));

  }

  /* (non-Javadoc)
   * @see Snake#getHead()
   */
  public Point getHead() {
    return snake.firstElement();
  }

  /* (non-Javadoc)
   * @see Snake#getTail()
   */
  public Point getTail() {
    return snake.lastElement();
  }

  /* (non-Javadoc)
   * @see Snake#changeDirection(java.lang.String)
   */
  public void changeDirection(String aDirection) {
    moveManager.changeDirection(aDirection);
  }

  /* (non-Javadoc)
   * @see Snake#move()
   */
  public void move() {
    Point prevTail = getTail();
    if (bodySize == snake.size()) {

      snake.remove(snake.lastElement());


    } else if (bodySize < snake.size()) {

      for (int i = 0; i < bodySize - snake.size(); i++) {
        snake.remove(snake.lastElement());
      }

    }

    snake.add(0, moveManager.move(getHead()));

    fireSnakeMoved(new SnakeMoveEvent(this, getHead(), prevTail));

  }

  /* (non-Javadoc)
   * @see Snake#addSnakeMoveEventListener(SnakeMoveEventListener)
   */
  public void addSnakeMoveEventListener(SnakeMoveEventListener listener) {
    listenerList.add(SnakeMoveEventListener.class, listener);
  }

  /* (non-Javadoc)
   * @see Snake#removeSnakeMoveEventListener(SnakeMoveEventListener)
   */
  public void removeSnakeMoveEventListener(SnakeMoveEventListener listener) {
    listenerList.remove(SnakeMoveEventListener.class, listener);
  }

  private void addSnakeWasCreatedListener(SnakeWasCreatedEventListener listener) {
    listenerList.add(SnakeWasCreatedEventListener.class, listener);
  }

  private void fireSnakeMoved(SnakeMoveEvent evt) {
    Object[] listeners = listenerList.getListenerList();
    for (int i = 0; i < listeners.length; i += 2) {
      if (listeners[i] == SnakeMoveEventListener.class) {
        ((SnakeMoveEventListener) listeners[i + 1]).snakeMoved(evt);
      }
    }
  }

  private void fireSnakeCreated(SnakeWasCreatedEvent evt) {
    Object[] listeners = listenerList.getListenerList();
    for (int i = 0; i < listeners.length; i += 2) {
      if (listeners[i] == SnakeWasCreatedEventListener.class) {
        ((SnakeWasCreatedEventListener) listeners[i + 1]).snakeWasCreated(evt);
      }
    }
  }

  /* (non-Javadoc)
   * @see Snake#grow()
   */
  public void grow() {
    bodySize++;
  }

  /* (non-Javadoc)
   * @see Snake#reduction()
   */
  public void reduction() {
    bodySize--;
  }

  /* (non-Javadoc)
   * @see Snake#getSize()
   */
  public int getSize() {
    return bodySize;
  }

}