package eu.tsamek.pro3.snake_v1.comm_v1.impl;

import java.io.Serializable;
import java.util.Map;

/**
 * @author Tom� Samek
 * T��da p�edstavuj�c� standartn� komunika�n� bal�k pomoc� n�ho� klient
 * komunikuje se serverem
 */
public class Packet implements Serializable {

  private static final long serialVersionUID = -6893405599939783432L;
  protected Map data;
  protected int status;

  /**
   * Defaultn� konstruktor
   *
   * @param data
   * @param status
   */
  public Packet(Map data, int status) {
    this.data = data;
    this.status = status;
  }

  public Map getData() {
    return data;
  }

  public int getStatus() {
    return status;
  }

}