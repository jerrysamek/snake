package eu.tsamek.pro3.snake_v1.core_v1.events_v1.impl;

import java.awt.*;
import java.util.EventObject;
import java.util.Vector;

/**
 * @author Tom� Samek
 * Ud�lost vyvolan� vytvo�en�m hada
 */
public class SnakeWasCreatedEvent extends EventObject {

  private static final long serialVersionUID = -5084143880788005600L;
  Vector<Point> body;

  public SnakeWasCreatedEvent(Object source, Vector<Point> body) {
    super(source);
    this.body = body;
  }

  /**
   * @return cel� t�lo hadovo
   */
  public Vector<Point> getBody() {
    return body;
  }


}
