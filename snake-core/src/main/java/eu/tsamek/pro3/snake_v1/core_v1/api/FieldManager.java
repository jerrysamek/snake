package eu.tsamek.pro3.snake_v1.core_v1.api;

import java.util.Map;

/**
 * @author Tom� Samek
 * <p>
 * slou�� k vytv��en� objekt� typu Field
 * @version 1.0
 * @created 24-VIII-2007 18:30:05
 */
public interface FieldManager {

  String FIELD_OBSTRUCTION = "Field.Obstruction";
  String FIELD_SNAKEFOOD = "Field.SnakeFood";
  String FIELD_SNAKEPART = "Field.SnakePart";

  String FIELD_OBSTRUCTION_CLASS = "Obstruction";
  String FIELD_SNAKEFOOD_CLASS = "Food";
  String FIELD_SNAKEPART_CLASS = "SnakePart";

  String CLASS_PREFIX = "net.jerryproject.";
  String CONST_POSTFIX = "_CLASS";


  /**
   * vytv��� pole bez vstupn�ch parametr�
   *
   * @param aName n�zev pole
   * @return Field
   */
  Field createField(String aName);

  /**
   * vytv��� pole ze vstupn�ch parametr�
   *
   * @param aName   n�zev pole
   * @param aParams parametry pole
   * @return Field
   */
  Field createField(String aName, Map<String, Object> aParams);

}