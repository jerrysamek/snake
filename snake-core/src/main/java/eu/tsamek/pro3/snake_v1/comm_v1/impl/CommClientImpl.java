package eu.tsamek.pro3.snake_v1.comm_v1.impl;

import eu.tsamek.pro3.snake_v1.comm_v1.api.CommClient;
import eu.tsamek.pro3.snake_v1.comm_v1.events_1.impl.SnakeCommunicationEvent;
import eu.tsamek.pro3.snake_v1.comm_v1.listeners_v1.api.SnakeCommunicationEventListener;
import eu.tsamek.pro3.snake_v1.core_v1.impl.AbstractComponent;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.EventListener;
import java.util.EventObject;
import java.util.Map;

/**
 * @author Tom� Samek
 * Implementace komunika�n�ho klienta
 */
public class CommClientImpl extends AbstractComponent implements CommClient {

  private static final long serialVersionUID = -4928229966187223185L;

  protected Packet packet = null;
  protected int status = 0;
  protected int clientIdentity = -1;
  protected ObjectOutputStream outputStream;
  protected Socket socket;
  protected ClientThread receiver;
  protected javax.swing.event.EventListenerList listenerList =
      new javax.swing.event.EventListenerList();

  /**
   * standartn� konstruktor komponenty
   *
   * @param attributes mapa parametr� po�adov�no A_PORT (ozna�uj�c� port serveru) a A_HOST (ozna�uj�c� server)
   * @throws UnknownHostException
   * @throws IOException
   */
  public CommClientImpl(Map<String, Object> attributes) throws UnknownHostException, IOException {
    super(attributes);

    int port = (Integer) attributes.get(A_PORT);
    String host = (String) attributes.get(A_HOST);

    connect(host, port);
  }

  /* (non-Javadoc)
   * @see CommClient#connect(java.lang.String, int)
   */
  public void connect(String host, int port) throws IOException {
    socket = new Socket(host, port);

    outputStream = new ObjectOutputStream(socket.getOutputStream());

    initReceiver();

  }

  /**
   * inicializuje vl�kno staraj�c� se o p��jem dat ze serveru
   *
   * @throws IOException
   */
  protected void initReceiver() throws IOException {
    receiver = new ClientThread(new ObjectInputStream(socket.getInputStream()));
    receiver.addClientThreadListener(new ClientThreadListener() {

      public void dataReceived(DataReceivedEvent evt) {
        switch (evt.getPacket().getStatus()) {
          case SERVERSTATUS_DATA:
            fireSnakeDataReceivedEvent(new SnakeCommunicationEvent(this, evt.getPacket().getData()));
            break;
          case SERVERSTATUS_IDENTIFICATION:
            Map m = evt.getPacket().getData();
            clientIdentity = (Integer) m.get("identity");
            fireSnakeServerConnectedEvent(new SnakeCommunicationEvent(this, evt.getPacket().getData()));
            status = SERVERSTATUS_DATA;
            break;
          case SERVERSTATUS_DISCONNECT:
            try {
              outputStream.close();
              receiver.stopReceiver();
              socket.close();

            } catch (IOException e) {
              // TODO Auto-generated catch block
              e.printStackTrace();
            }
            fireSnakeServerDisconnectedEvent();
            break;
        }
      }

    });

    receiver.start();
  }

  /* (non-Javadoc)
   * @see CommClient#disconnect()
   */
  public synchronized void disconnect() throws IOException {
    packet = new Packet(null, CLIENTSTATUS_DISCONNECT);
    sendPacket();

  }

  /* (non-Javadoc)
   * @see CommClient#addCommunicationEventListener(SnakeCommunicationEventListener)
   */
  public void addCommunicationEventListener(SnakeCommunicationEventListener listener) {
    listenerList.add(SnakeCommunicationEventListener.class, listener);
  }

  /* (non-Javadoc)
   * @see CommClient#removeCommunicationEventListener(SnakeCommunicationEventListener)
   */
  public void removeCommunicationEventListener(SnakeCommunicationEventListener listener) {
    listenerList.remove(SnakeCommunicationEventListener.class, listener);
  }

  private void fireSnakeDataReceivedEvent(SnakeCommunicationEvent evt) {
    Object[] listeners = listenerList.getListenerList();
    for (int i = 0; i < listeners.length; i += 2) {
      if (listeners[i] == SnakeCommunicationEventListener.class) {
        ((SnakeCommunicationEventListener) listeners[i + 1]).dataReceived(evt);
      }
    }
  }

  private void fireSnakeServerDisconnectedEvent() {
    Object[] listeners = listenerList.getListenerList();
    for (int i = 0; i < listeners.length; i += 2) {
      if (listeners[i] == SnakeCommunicationEventListener.class) {
        ((SnakeCommunicationEventListener) listeners[i + 1]).serverDisconnected();
      }
    }
  }

  private void fireSnakeServerConnectedEvent(SnakeCommunicationEvent evt) {
    Object[] listeners = listenerList.getListenerList();
    for (int i = 0; i < listeners.length; i += 2) {
      if (listeners[i] == SnakeCommunicationEventListener.class) {
        ((SnakeCommunicationEventListener) listeners[i + 1]).serverConnected(evt);
      }
    }
  }

  /* (non-Javadoc)
   * @see CommClient#createPacket(java.util.Map)
   */
  public synchronized void createPacket(Map data) {
    packet = new Packet(data, status);
  }

  /* (non-Javadoc)
   * @see CommClient#sendPacket()
   */
  public synchronized void sendPacket() throws IOException {
    if (outputStream != null) {

      outputStream.writeObject(packet);
    }
  }

  /* (non-Javadoc)
   * @see CommClient#sendServerDisconnectRequest()
   */
  public synchronized void sendServerDisconnectRequest() throws IOException {
    outputStream.writeObject(new Packet(null, SERVERSTATUS_DISCONNECT));
  }

  /**
   * @author Tom� Samek
   * interface intern�ho listeneru pro p��jem dat
   */
  private interface ClientThreadListener extends EventListener {

    void dataReceived(DataReceivedEvent evt);

  }

  /**
   * @author Tom� Samek
   * Intern� ud�lost vyvolan� p�i p�ijet� dat ze serveru.
   */
  protected class DataReceivedEvent extends EventObject {


    private static final long serialVersionUID = 1L;
    protected Packet packet;

    public DataReceivedEvent(Object source, Packet packet) {
      super(source);
      this.packet = packet;
    }

    public Packet getPacket() {
      return packet;
    }

  }

  /**
   * @author Tom� Samek
   * intern� vl�kno kter� se star� o p��jem dat ze serveru
   */
  protected class ClientThread extends Thread {

    protected ObjectInputStream inputStream;
    protected javax.swing.event.EventListenerList listenerList =
        new javax.swing.event.EventListenerList();
    private boolean rStop = false;

    /**
     * defaultn� konstruktor vl�kna
     *
     * @param inputStream input stream ze socketu
     */
    public ClientThread(ObjectInputStream inputStream) {
      this.inputStream = inputStream;
    }

    /**
     * vypne vl�kno
     */
    public void stopReceiver() {
      rStop = true;
    }

    @Override
    public void run() {

      try {
        Packet packet;

        while (!rStop && !socket.isClosed() && !socket.isInputShutdown() && (packet = (Packet) inputStream.readObject()) != null) {

          fireDataReceivedEvent(new DataReceivedEvent(this, packet));

        }
        inputStream.close();
      } catch (IOException e) {
        if (e instanceof EOFException) {
          System.out.println("End of stream");
        } else {
          e.printStackTrace();
        }

      } catch (ClassNotFoundException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }

    }

    /**
     * p�id� nov� listener pro vl�kno
     *
     * @param listener ClientThreadListener
     */
    public void addClientThreadListener(ClientThreadListener listener) {
      listenerList.add(ClientThreadListener.class, listener);
    }

    /**
     * odebere listener z vl�kna
     *
     * @param listener ClientThreadListener
     */
    public void removeClientThreadListener(ClientThreadListener listener) {
      listenerList.remove(ClientThreadListener.class, listener);
    }

    private void fireDataReceivedEvent(DataReceivedEvent evt) {
      Object[] listeners = listenerList.getListenerList();
      for (int i = 0; i < listeners.length; i += 2) {
        if (listeners[i] == ClientThreadListener.class) {
          ((ClientThreadListener) listeners[i + 1]).dataReceived(evt);
        }
      }
    }
  }


}
