package eu.tsamek.pro3.snake_v1.core_v1.events_v1.impl;

import java.awt.*;
import java.util.EventObject;

/**
 * @author Tom� Samek
 * Ud�lost vyvolan� koliz� hada
 */
public class SnakeCollisionEvent extends EventObject {

  private static final long serialVersionUID = -1390566448184672717L;

  String collision;
  Point position;

  public SnakeCollisionEvent(Object source, String collision, Point position) {
    super(source);
    this.collision = collision;
    this.position = position;
  }

  /**
   * @return vrac� typ kolize
   */
  public String getCollision() {
    return collision;
  }

  /**
   * @return vrac� pozici kolize
   */
  public Point getPosition() {
    return position;
  }

}
