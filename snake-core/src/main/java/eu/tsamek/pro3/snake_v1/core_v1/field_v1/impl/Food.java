package eu.tsamek.pro3.snake_v1.core_v1.field_v1.impl;

import eu.tsamek.pro3.snake_v1.gui_v1.render_v1.api.SnakeRender;

import java.util.Map;

/**
 * @author Tom� Samek
 * @version 1.0
 * @created 24-VIII-2007 18:30:06
 */
public class Food extends AbstractField {

  private static final long serialVersionUID = 7779330180990076826L;

  /**
   * {@inheritDoc}
   */
  public Food(Map<String, Object> attributes) {
    super(attributes);
  }

  /* (non-Javadoc)
   * @see Field#getRender()
   */
  public SnakeRender getRender() {

    return render;
  }

  /* (non-Javadoc)
   * @see Field#setRender(SnakeRender)
   */
  public void setRender(SnakeRender render) {
    this.render = render;

  }


}