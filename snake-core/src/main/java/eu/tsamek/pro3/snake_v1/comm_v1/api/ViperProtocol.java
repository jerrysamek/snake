package eu.tsamek.pro3.snake_v1.comm_v1.api;

public interface ViperProtocol {
  int CLIENTSTATUS_DISCONNECT = 5;
  int CLIENTSTATUS_OK = 4;
  int SERVERSTATUS_DISCONNECT = 3;
  int SERVERSTATUS_DATA = 2;
  int SERVERSTATUS_IDENTIFICATION = 1;

  String DATA_STARTDIRECTION = "startdirection";
  String DATA_STARTPOSITION = "startposition";
  String DATA_IDENTITY = "identity";
  String DATA_START = "start";
}
