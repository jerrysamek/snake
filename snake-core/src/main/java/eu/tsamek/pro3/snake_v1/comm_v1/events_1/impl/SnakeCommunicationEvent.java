package eu.tsamek.pro3.snake_v1.comm_v1.events_1.impl;

import java.util.EventObject;
import java.util.Map;

/**
 * @author Tom� Samek
 * Ud�lost vyvolan� klientem p�i p�ijet� dat
 */
public class SnakeCommunicationEvent extends EventObject {

  private static final long serialVersionUID = 2680998796964380783L;

  private Map data;

  public SnakeCommunicationEvent(Object source, Map data) {
    super(source);

    this.data = data;

  }

  /**
   * @return vrac� data z packetu
   */
  public Map getData() {
    return data;
  }


}
