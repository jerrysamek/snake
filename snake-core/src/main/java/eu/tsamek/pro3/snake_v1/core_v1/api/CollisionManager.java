package eu.tsamek.pro3.snake_v1.core_v1.api;

/**
 * @author Tom� Samek
 * @version 1.0
 * @created 24-VIII-2007 18:30:04
 */
public interface CollisionManager {

  String COLLISION_OBSTRUCTION = "Obstruction";
  String COLLISION_SNAKE_FOOD = "SnakeFood";
  String COLLISION_SNAKE_PART = "SnakePart";

  /**
   * Vrac� kolizi pokud nastane
   *
   * @param aField
   */
  String detectCollision(Field aField);

}