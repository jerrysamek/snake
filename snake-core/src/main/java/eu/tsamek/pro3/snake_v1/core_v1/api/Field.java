package eu.tsamek.pro3.snake_v1.core_v1.api;

import eu.tsamek.pro3.snake_v1.gui_v1.render_v1.api.SnakeRender;

/**
 * @author Tom� Samek
 * @version 1.0
 * @created 24-VIII-2007 18:30:05
 */
public interface Field {

  String A_RENDER = "Field.Render";

  /**
   * @return vrac� t��du kter� je schopna vyrendrovat dan� pole
   */
  SnakeRender getRender();

  /**
   * nastavuje rendr pro dan� pole
   *
   * @param render implementace interfacu SnakeRender
   */
  void setRender(SnakeRender render);

}