package eu.tsamek.pro3.snake_v1.comm_v1.api;

import eu.tsamek.pro3.snake_v1.comm_v1.listeners_v1.api.SnakeCommunicationEventListener;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Map;

/**
 * @author Tom� Samek
 * Komunika�n� klient by m�l vych�zet ze seznamu k�d� pro komunikaci
 * uveden�ch v interfacu ViperProtocol
 */
public interface CommClient extends ViperProtocol {
  String A_HOST = "Comm.Host";
  String A_PORT = "Comm.Port";

  /**
   * p�ipojuje se k serveru
   *
   * @param host
   * @param port
   * @throws UnknownHostException
   * @throws IOException
   */
  void connect(String host, int port) throws UnknownHostException, IOException;

  /**
   * odpojuje se od servru
   *
   * @throws IOException
   */
  void disconnect() throws IOException;

  /**
   * odes�l� servru po�adavek na jeho uko�en�
   *
   * @throws IOException
   */
  void sendServerDisconnectRequest() throws IOException;

  /**
   * vytv��� packet
   *
   * @param data Mapa dat
   */
  void createPacket(Map data);

  /**
   * odes�l� vytvo�en� packet
   *
   * @throws IOException
   */
  void sendPacket() throws IOException;

  /**
   * p�id�v� listener kter� odposlouch�v� ud�losti na komponent�
   *
   * @param listener
   */
  void addCommunicationEventListener(SnakeCommunicationEventListener listener);

  /**
   * odeb�r� listener kter� odposlouch�v� ud�losti na komponent�
   *
   * @param listener
   */
  void removeCommunicationEventListener(SnakeCommunicationEventListener listener);

}
