package eu.tsamek.pro3.snake_v1.gui_v1.impl;

import eu.tsamek.pro3.snake_v1.core_v1.api.Field;
import eu.tsamek.pro3.snake_v1.core_v1.api.Grid;
import eu.tsamek.pro3.snake_v1.core_v1.events_v1.impl.SnakeCollisionEvent;
import eu.tsamek.pro3.snake_v1.core_v1.events_v1.impl.SnakeFieldEvent;
import eu.tsamek.pro3.snake_v1.core_v1.listeners_v1.api.SnakeFieldEventListener;
import eu.tsamek.pro3.snake_v1.gui_v1.render_v1.api.SnakeRender;
import eu.tsamek.pro3.snake_v1.gui_v1.render_v1.impl.DefaultRender;

import javax.swing.*;
import java.awt.*;

/**
 * @author Tom� Samek
 * Slou�� k vykreslov�n� modelu pole
 * @version 1.0
 * @created 24-VIII-2007 18:30:06
 */
public class GridGUI extends JPanel {

  private static final long serialVersionUID = -7859252494435128185L;
  Point redrawPoint = null;
  private Grid grid;
  private SnakeRender render;
  private boolean reDrawAble = true;

  /**
   * defaultn� konstruktor
   *
   * @param aGrid           model pole
   * @param aFieldDimension rozm�r pol��ka
   */
  public GridGUI(Grid aGrid, Dimension aFieldDimension) {
    this.grid = aGrid;
    render = new DefaultRender(aFieldDimension);

    setSize(getPXSize(aFieldDimension));
    setPreferredSize(getPXSize(aFieldDimension));
    setMinimumSize(getPXSize(aFieldDimension));
    setMaximumSize(getPXSize(aFieldDimension));

    lockRepaint();
    repaint();

    installListeners();
  }

  /**
   * kontroluje jestli je mo�n� p�ekreslen�
   */
  private void checkLock() {
    while (!reDrawAble) {
      try {
        Thread.sleep(1);
      } catch (InterruptedException e) {}
    }
  }

  /**
   * p�ekresl� cel� pole bez vyj�mky
   */
  public synchronized void repaintAll() {
    lockRepaint();
    redrawPoint = null;
    repaint();
  }

  /**
   * uzamkne p�ekreslov�n�
   */
  private void lockRepaint() {
    reDrawAble = false;
  }

  /**
   * odemkne p�ekreslov�n�
   */
  private void unlockRepaint() {
    reDrawAble = true;
  }

  /**
   * @param aFieldDimension rozm�r pol��ka
   * @return vrac� rozm�r pole v pixlech
   */
  private Dimension getPXSize(Dimension aFieldDimension) {

    return new Dimension(grid.getSize().width * aFieldDimension.width, grid.getSize().height * aFieldDimension.height);

  }

  /**
   * instaluje listener na model pole
   */
  private void installListeners() {
    grid.addSnakeFieldEventListener(new SnakeFieldEventListener() {

      /* (non-Javadoc)
       * @see SnakeFieldEventListener#snakeFieldAdded(SnakeFieldEvent)
       */
      public void snakeFieldAdded(SnakeFieldEvent evt) {
        checkLock();

        lockRepaint();
        redrawPoint = evt.getPart();
        repaint();

      }

      /* (non-Javadoc)
       * @see SnakeFieldEventListener#snakeFieldRemoved(SnakeFieldEvent)
       */
      public void snakeFieldRemoved(SnakeFieldEvent evt) {
        checkLock();

        lockRepaint();
        redrawPoint = evt.getPart();
        repaint();

      }

      /* (non-Javadoc)
       * @see SnakeFieldEventListener#snakeFieldCollision(SnakeCollisionEvent)
       */
      public void snakeFieldCollision(SnakeCollisionEvent evt) {

      }

      /* (non-Javadoc)
       * @see SnakeFieldEventListener#snakeDataChanged()
       */
      public void snakeDataChanged() {
        repaintAll();
      }
    });
  }


  /**
   * @param g
   */
  public synchronized void paint(Graphics g) {

	/*	if(redrawPoint != null){
			Field field = grid.getField(redrawPoint);
			
			render.renderPart(g, redrawPoint);
			
			if (field != null) {
				field.getRender().renderPart(g, redrawPoint);
			}
			redrawPoint = null;
		}else{
		*/
    for (int i = 0; i < grid.getSize().width; i++) {
      for (int j = 0; j < grid.getSize().height; j++) {
        Field field = grid.getField(new Point(i, j));

        render.renderPart(g, new Point(i, j));

        if (field != null) {
          field.getRender().renderPart(g, new Point(i, j));
        }
      }
    }
    //	}
    unlockRepaint();
  }


  /**
   * nastavuje pole
   *
   * @param aGrid model pole
   */
  public void setGrid(Grid aGrid) {
    this.grid = aGrid;
    installListeners();

  }

  /**
   * @return vrac� defaultn� renderer
   */
  public SnakeRender getDefaultRender() {
    return render;
  }

  /**
   * nastavuje defaultn� renderer
   *
   * @param render nov� renderer
   */
  public void setDefaultRender(SnakeRender render) {
    this.render = render;
    repaint();
  }


}