package eu.tsamek.pro3.snake_v1.core_v1.action_v1.impl;

import eu.tsamek.pro3.snake_v1.core_v1.api.ActionBuffer;
import eu.tsamek.pro3.snake_v1.core_v1.api.ActionManager;
import eu.tsamek.pro3.snake_v1.core_v1.impl.AbstractComponent;
import eu.tsamek.pro3.snake_v1.core_v1.impl.ComponentFactory;

import java.awt.event.KeyEvent;
import java.util.Map;

/**
 * @author Tom� Samek
 * <p>
 * implementace interfacu ActionManager, kter� je uzp�sobena
 * pro odposlouch�v�n� akc� vyvolan�ch u�ivatelem na kl�vesnici
 * @version 1.0
 * @created 24-VIII-2007 18:30:04
 */
public class ActionManagerImpl extends AbstractComponent implements ActionManager {

  private static final long serialVersionUID = -3181207383712971844L;
  private ActionBuffer actionBuffer =
      (ActionBuffer) ComponentFactory.createComponent(
          ComponentFactory.COM_ACTIONBUFFER);

  /**
   * @param attributes null
   */
  public ActionManagerImpl(Map<String, Object> attributes) {
    super(attributes);

  }

  /* (non-Javadoc)
   * @see ActionManager#getAction()
   */
  public String getAction() {
    return actionBuffer.getAction();
  }


  /* (non-Javadoc)
   * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
   */
  public void keyPressed(KeyEvent e) {
    switch (e.getKeyCode()) {
      case KeyEvent.VK_UP:
        actionBuffer.addAction(ACT_KEY_UP);
        break;
      case KeyEvent.VK_DOWN:
        actionBuffer.addAction(ACT_KEY_DOWN);
        break;
      case KeyEvent.VK_LEFT:
        actionBuffer.addAction(ACT_KEY_LEFT);
        break;

      case KeyEvent.VK_RIGHT:
        actionBuffer.addAction(ACT_KEY_RIGHT);
        break;
      default:
        break;
    }
  }

  /* (non-Javadoc)
   * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
   */
  public void keyReleased(KeyEvent e) {

  }

  /* (non-Javadoc)
   * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
   */
  public void keyTyped(KeyEvent e) {

  }


}