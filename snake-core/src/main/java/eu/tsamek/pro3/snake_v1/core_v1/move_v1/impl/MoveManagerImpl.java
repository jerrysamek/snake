package eu.tsamek.pro3.snake_v1.core_v1.move_v1.impl;

import eu.tsamek.pro3.snake_v1.core_v1.api.MoveManager;
import eu.tsamek.pro3.snake_v1.core_v1.impl.AbstractComponent;

import java.awt.*;
import java.util.Map;

/**
 * @author Tom� Samek
 * @version 1.0
 * @created 24-VIII-2007 18:30:07
 */
public class MoveManagerImpl extends AbstractComponent implements MoveManager {

  private static final long serialVersionUID = 4240764226923138803L;

  private static final int DIR_BACKWARD = -1;
  private static final int DIR_FORWARD = 1;
  private String direction = DIR_RIGHT;

  /**
   * @param attributes null
   */
  public MoveManagerImpl(Map<String, Object> attributes) {
    super(attributes);
  }

  /* (non-Javadoc)
   * @see MoveManager#changeDirection(java.lang.String)
   */
  public void changeDirection(String aDirection) {
    direction = aDirection;
  }


  /* (non-Javadoc)
   * @see MoveManager#move(java.awt.Point)
   */
  public Point move(Point aActualPosition) {
    return moving(aActualPosition, DIR_FORWARD);
  }

  /**
   * vnit�n� funkce pou��van� pro pohyb
   *
   * @param aActualPosition aktu�ln� pozice
   * @param aDirection      sm�r pohybu - dop�edu / dozadu
   * @return Point nov� pozice
   */
  private Point moving(Point aActualPosition, int aDirection) {
    Point newPosition = new Point(aActualPosition);

    if (direction.equals(DIR_DOWN)) {
      newPosition.x = newPosition.x + MOVE_DOWN.x * aDirection;
      newPosition.y = newPosition.y + MOVE_DOWN.y * aDirection;
      return newPosition;
    }
    if (direction.equals(DIR_UP)) {
      newPosition.x = newPosition.x + MOVE_UP.x * aDirection;
      newPosition.y = newPosition.y + MOVE_UP.y * aDirection;
      return newPosition;
    }
    if (direction.equals(DIR_LEFT)) {
      newPosition.x = newPosition.x + MOVE_LEFT.x * aDirection;
      newPosition.y = newPosition.y + MOVE_LEFT.y * aDirection;
      return newPosition;
    }
    if (direction.equals(DIR_RIGHT)) {
      newPosition.x = newPosition.x + MOVE_RIGHT.x * aDirection;
      newPosition.y = newPosition.y + MOVE_RIGHT.y * aDirection;
      return newPosition;
    }

    return null;
  }

  /* (non-Javadoc)
   * @see MoveManager#moveBack(java.awt.Point)
   */
  public Point moveBack(Point aActualPosition) {
    return moving(aActualPosition, DIR_BACKWARD);
  }


}