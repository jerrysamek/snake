package eu.tsamek.pro3.snake_v1.core_v1.listeners_v1.api;

import eu.tsamek.pro3.snake_v1.core_v1.events_v1.impl.SnakeMoveEvent;

import java.util.EventListener;

/**
 * @author Tom� Samek
 */
public interface SnakeMoveEventListener extends EventListener {

  /**
   * ud�lost vyvolan� pohybem hada
   *
   * @param evt SnakeMoveEvent
   */
  void snakeMoved(SnakeMoveEvent evt);


}
