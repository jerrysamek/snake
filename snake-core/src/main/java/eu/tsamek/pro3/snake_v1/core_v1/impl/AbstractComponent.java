package eu.tsamek.pro3.snake_v1.core_v1.impl;

import eu.tsamek.pro3.snake_v1.core_v1.api.Component;

import java.util.Map;

/**
 * @version 1.0
 * @created 24-VIII-2007 18:30:03
 */
public abstract class AbstractComponent implements Component {

  /**
   * standartn� konstruktor komponenty
   *
   * @param attributes mapa parametr�
   */
  public AbstractComponent(Map<String, Object> attributes) {
  }

}