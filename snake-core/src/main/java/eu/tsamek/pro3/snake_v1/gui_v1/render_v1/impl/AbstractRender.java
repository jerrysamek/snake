package eu.tsamek.pro3.snake_v1.gui_v1.render_v1.impl;

import eu.tsamek.pro3.snake_v1.gui_v1.render_v1.api.SnakeRender;

import java.awt.*;

/**
 * @author Tom� Samek
 * Abstraktn� render ukl�d�j�c� si pouze dimenzi objektu a
 * nastavuje kvalitu vykreslov�n�
 */
public abstract class AbstractRender implements SnakeRender {

  Dimension d;

  /**
   * defaultn� konstruktor
   *
   * @param d rozm�r pol��ka
   */
  public AbstractRender(Dimension d) {
    this.d = d;
  }

  final public void renderPart(Graphics g, Point position) {
    Graphics2D g2d = (Graphics2D) g;

    g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    g2d.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);

    renderPart(g2d, position);
  }

  /**
   * vykresluje pol��ko na pozici
   *
   * @param g2d      Graphics2D
   * @param position pozice pol��ka
   */
  protected abstract void renderPart(Graphics2D g2d, Point position);
}
