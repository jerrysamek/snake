package eu.tsamek.pro3.snake_v1.gui_v1.render_v1.impl;

import java.awt.*;

/**
 * @author Tom� Samek
 * Vykresluje p�ek�ku
 */
public class ObstructionRender extends AbstractRender {

  /**
   * {@inheritDoc}
   */
  public ObstructionRender(Dimension d) {
    super(d);

  }

  /* (non-Javadoc)
   * @see AbstractRender#renderPart(java.awt.Graphics2D, java.awt.Point)
   */
  @Override
  public void renderPart(Graphics2D g2d, Point position) {

    Color startColor = Color.black;
    Color endColor = Color.gray;

    GradientPaint gradient = new GradientPaint((int) ((double) position.x + 0.5) * d.width,
        (int) ((double) position.y + 0.5) * d.height,
        startColor,
        (position.x + 1) * d.width - 1,
        (position.y + 1) * d.height - 1,
        endColor,
        true);

    g2d.setPaint(gradient);

    g2d.fillRect(position.x * d.width, position.y * d.height, d.width - 1, d.height - 1);


  }
}
