package eu.tsamek.pro3.snake_v1.core_v1.api;

import java.awt.*;

/**
 * @author Tom� Samek
 * Slou�� pro organizaci pohybu na plo�e
 * @version 1.0
 * @created 24-VIII-2007 18:30:06
 */
public interface MoveManager {

  String DIR_DOWN = "Direction.Down";
  String DIR_LEFT = "Direction.Left";
  String DIR_RIGHT = "Direction.Right";
  String DIR_UP = "Direction.Up";
  Point MOVE_DOWN = new Point(0, 1);
  Point MOVE_LEFT = new Point(-1, 0);
  Point MOVE_RIGHT = new Point(1, 0);
  Point MOVE_UP = new Point(0, -1);

  /**
   * Zm�n� sm�r pohybu hada
   *
   * @param aDirection String
   */
  void changeDirection(String aDirection);

  /**
   * Pohne hadem v nastaven�m sm�ru
   *
   * @param aActualPosition aktu�ln� pozice hlavy hada
   * @return nov� pozice
   */
  Point move(Point aActualPosition);

  /**
   * Pohne hadem v opa�n�m ne� nastaven�m sm�ru
   *
   * @param aActualPosition aktu�ln� pozice hlavy hada
   * @return nov� pozice
   */
  Point moveBack(Point aActualPosition);

}