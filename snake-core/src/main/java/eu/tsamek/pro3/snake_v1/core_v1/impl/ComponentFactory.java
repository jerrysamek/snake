package eu.tsamek.pro3.snake_v1.core_v1.impl;

import eu.tsamek.pro3.snake_v1.core_v1.api.Component;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;


/**
 * @version 1.0
 * @created 24-VIII-2007 18:30:05
 * T��da slou�� pro vytv��en� objekt� pod�d�n�ch z objektu AbstractComponent
 */
public class ComponentFactory {

  public static final String COM_ACTIONBUFFER = "Component.ActionBuffer";
  public static final String COM_ACTIONMANAGER = "Component.ActionManager";
  public static final String COM_COLLISIONMANAGER = "Component.CollisionManager";
  public static final String COM_FIELDMANAGER = "Component.FieldManager";
  public static final String COM_GRID = "Component.Grid";
  public static final String COM_MOVEMANAGER = "Component.MoveManager";
  public static final String COM_SNAKE = "Component.Snake";
  public static final String COM_COMMCLIENT = "Component.CommClient";
  public static final String COM_ACTIONBUFFER_CLASS = "ActionBufferImpl";
  public static final String COM_ACTIONMANAGER_CLASS = "ActionManagerImpl";
  public static final String COM_COLLISIONMANAGER_CLASS = "CollisionManagerImpl";
  public static final String COM_FIELDMANAGER_CLASS = "FieldManagerImpl";
  public static final String COM_GRID_CLASS = "GridImpl";
  public static final String COM_MOVEMANAGER_CLASS = "MoveManagerImpl";
  public static final String COM_SNAKE_CLASS = "SnakeImpl";
  public static final String COM_COMMCLIENT_CLASS = "CommClientImpl";
  private static final String CLASS_COMPONENTMANAGER = "ComponentFactory";
  private static final String CONST_POSTFIX = "_CLASS";
  private static final String CLASS_PREFIX = "net.jerryproject.";

  /**
   * vytv��� komponentu podle n�zvu
   *
   * @param aName n�zev komponenty
   * @return komponenta
   */
  public static Component createComponent(String aName) {
    return createComponent(aName, null);
  }

  /**
   * vytv��� komponentu podle n�zvu a zadan�ch paramentr�
   *
   * @param aName   n�zev komponenty
   * @param aParams parametry
   * @return komponenta
   */
  @SuppressWarnings("unchecked")
  public static Component createComponent(String aName, Map<String, Object> aParams) {
    try {

      Class<ComponentFactory> thisClass = (Class<ComponentFactory>) Class.forName(CLASS_COMPONENTMANAGER);

      Field[] fields = thisClass.getFields();
      String className = null;
      int i = 0;
      while (i < fields.length && className == null) {
        if (fields[i].get(fields[i].getName()).equals(aName)) {

          Field field = thisClass.getField(fields[i].getName().concat(CONST_POSTFIX));
          className = field.get(field.getName()).toString();

        }
        i++;
      }
      if (className == null) {
        throw new NoSuchFieldException();
      }
      Class<Component> clazz = (Class<Component>) Class.forName(CLASS_PREFIX.concat(className));
      Constructor<Component> constructor = clazz.getConstructor(Map.class);

      return constructor.newInstance(aParams);

    } catch (SecurityException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (ClassNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (NoSuchMethodException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IllegalArgumentException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (InstantiationException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (InvocationTargetException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (NoSuchFieldException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return null;
  }
}