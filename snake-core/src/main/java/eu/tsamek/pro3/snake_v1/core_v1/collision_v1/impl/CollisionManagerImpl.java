package eu.tsamek.pro3.snake_v1.core_v1.collision_v1.impl;

import eu.tsamek.pro3.snake_v1.core_v1.api.CollisionManager;
import eu.tsamek.pro3.snake_v1.core_v1.api.Field;
import eu.tsamek.pro3.snake_v1.core_v1.field_v1.impl.Food;
import eu.tsamek.pro3.snake_v1.core_v1.field_v1.impl.Obstruction;
import eu.tsamek.pro3.snake_v1.core_v1.field_v1.impl.SnakePart;
import eu.tsamek.pro3.snake_v1.core_v1.impl.AbstractComponent;

import java.util.Map;

/**
 * @author Tom� Samek
 * @version 1.0
 * @created 24-VIII-2007 18:30:04
 */
public class CollisionManagerImpl extends AbstractComponent implements CollisionManager {

  private static final long serialVersionUID = 59877137074131226L;

  /**
   * @param attributes null
   */
  public CollisionManagerImpl(Map<String, Object> attributes) {
    super(attributes);
  }

  /* (non-Javadoc)
   * @see CollisionManager#detectCollision(Field)
   */
  public String detectCollision(Field aField) {

    if (aField instanceof SnakePart) {
      return COLLISION_SNAKE_PART;
    } else if (aField instanceof Food) {
      return COLLISION_SNAKE_FOOD;
    } else if (aField instanceof Obstruction) {
      return COLLISION_OBSTRUCTION;
    }

    return null;
  }

}