package eu.tsamek.pro3.snake_v1.gui_v1.impl;

import eu.tsamek.pro3.snake_v1.core_v1.api.Grid;
import eu.tsamek.pro3.snake_v1.core_v1.api.SnakeApplication;
import eu.tsamek.pro3.snake_v1.core_v1.events_v1.impl.SnakeScoreEvent;
import eu.tsamek.pro3.snake_v1.core_v1.listeners_v1.api.SnakeInfoChangeListener;

import javax.swing.*;
import java.awt.*;
import java.util.Iterator;


/**
 * @version 1.0
 * @created 24-VIII-2007 18:30:04
 * <p>
 * T��da obstar�vaj�c� okno hada
 */
public class AppGUI extends JFrame {

  private static final long serialVersionUID = 4990175302359229855L;

  private JLabel score = new JLabel("Score: 0");

  private JLabel scoreBoard = new JLabel(" ScoreBoard ");

  /**
   * defaultn� konstruktor
   *
   * @param app             SnakeApplication jak�koli aplikace kter� implementuje SnakeApplication
   * @param grid            model pole
   * @param aFieldDimension rozm�r jednoho pol��ka
   */
  public AppGUI(SnakeApplication app, Grid grid, Dimension aFieldDimension) {
    initGUI(grid, aFieldDimension);

    app.addSnakeInfoChangeListener(new SnakeInfoChangeListener() {

      public void scoreChange(SnakeScoreEvent evt) {
        score.setText("My score: ".concat(String.valueOf(evt.getScore())));
      }

      public void scoreBoardChange(final SnakeScoreEvent evt) {
        String text = "ScoreBoard >>> ";
        for (Iterator iter = evt.getScoreMap().keySet().iterator(); iter.hasNext(); ) {
          String element = (String) iter.next();

          text = text.concat(element + " = " + evt.getScoreMap().get(element));
          if (iter.hasNext()) {
            text = text.concat(" . . . ");
          }
        }

        scoreBoard.setText(text);
      }
    });
  }

  /**
   * inicializuje grafiku
   *
   * @param grid            model pole
   * @param aFieldDimension rozm�r jednoho pol��ka
   */
  private void initGUI(Grid grid, Dimension aFieldDimension) {

    setLayout(new BorderLayout());
    GridGUI gridGUI = new GridGUI(grid, aFieldDimension);
    scoreBoard.setPreferredSize(new Dimension(150, 25));
    scoreBoard.setMinimumSize(new Dimension(150, 25));
    scoreBoard.setMaximumSize(new Dimension(150, 25));
    add(scoreBoard, BorderLayout.SOUTH);
    add(score, BorderLayout.NORTH);
    add(gridGUI, BorderLayout.CENTER);

    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setResizable(false);

    pack();
    setVisible(true);

  }

}