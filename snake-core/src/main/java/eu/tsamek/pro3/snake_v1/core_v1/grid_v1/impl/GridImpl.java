package eu.tsamek.pro3.snake_v1.core_v1.grid_v1.impl;

import eu.tsamek.pro3.snake_v1.core_v1.api.CollisionManager;
import eu.tsamek.pro3.snake_v1.core_v1.api.Field;
import eu.tsamek.pro3.snake_v1.core_v1.api.Grid;
import eu.tsamek.pro3.snake_v1.core_v1.events_v1.impl.SnakeCollisionEvent;
import eu.tsamek.pro3.snake_v1.core_v1.events_v1.impl.SnakeFieldEvent;
import eu.tsamek.pro3.snake_v1.core_v1.field_v1.impl.Food;
import eu.tsamek.pro3.snake_v1.core_v1.impl.AbstractComponent;
import eu.tsamek.pro3.snake_v1.core_v1.impl.ComponentFactory;
import eu.tsamek.pro3.snake_v1.core_v1.listeners_v1.api.SnakeFieldEventListener;

import java.awt.*;
import java.util.Map;

/**
 * @author Tom� Samek
 * @version 1.0
 * @created 24-VIII-2007 18:30:06
 */
public class GridImpl extends AbstractComponent implements Grid {

  private static final long serialVersionUID = 5429117943882421783L;
  protected javax.swing.event.EventListenerList listenerList =
      new javax.swing.event.EventListenerList();
  private Field[][] field;
  private Dimension size;
  private CollisionManager collisionManager;

  /**
   * @param attributes A_DIMENSION obsahuje informace o rozm�rech pole
   */
  public GridImpl(Map<String, Object> attributes) {
    super(attributes);

    size = (Dimension) attributes.get(A_DIMENSION);
    field = new Field[size.width][size.height];

    collisionManager = (CollisionManager) ComponentFactory.createComponent(ComponentFactory.COM_COLLISIONMANAGER);

  }

  /* (non-Javadoc)
   * @see Grid#getField(java.awt.Point)
   */
  public Field getField(Point aPosition) {
    return field[aPosition.x][aPosition.y];

  }

  /* (non-Javadoc)
   * @see Grid#addField(java.awt.Point, Field)
   */
  public void addField(Point aPosition, Field aField) {
    String s = collisionManager.detectCollision(getField(aPosition));
    if (s == null) {

      field[aPosition.x][aPosition.y] = aField;
      fireFieldAdded(new SnakeFieldEvent(this, aPosition));

    } else {

      if (s.equals(CollisionManager.COLLISION_SNAKE_FOOD)) {

        field[aPosition.x][aPosition.y] = aField;
        fireFieldAdded(new SnakeFieldEvent(this, aPosition));
      }

      fireCollision(new SnakeCollisionEvent(this, s, aPosition));

    }
  }

  /* (non-Javadoc)
   * @see Grid#getSize()
   */
  public Dimension getSize() {
    return size;
  }

  /* (non-Javadoc)
   * @see Grid#removeField(java.awt.Point)
   */
  public void removeField(Point aPosition) {
    field[aPosition.x][aPosition.y] = null;
    fireFieldRemoved(new SnakeFieldEvent(this, aPosition));
  }

  /* (non-Javadoc)
   * @see Grid#addSnakeFieldEventListener(SnakeFieldEventListener)
   */
  public void addSnakeFieldEventListener(SnakeFieldEventListener listener) {
    listenerList.add(SnakeFieldEventListener.class, listener);
  }

  /* (non-Javadoc)
   * @see Grid#removeSnakeFieldEventListener(SnakeFieldEventListener)
   */
  public void removeSnakeFieldEventListener(SnakeFieldEventListener listener) {
    listenerList.remove(SnakeFieldEventListener.class, listener);
  }

  /* (non-Javadoc)
   * @see Grid#fireDataChangedEvent()
   */
  public void fireDataChangedEvent() {
    Object[] listeners = listenerList.getListenerList();
    for (int i = 0; i < listeners.length; i += 2) {
      if (listeners[i] == SnakeFieldEventListener.class) {
        ((SnakeFieldEventListener) listeners[i + 1]).snakeDataChanged();
      }
    }
  }

  private void fireCollision(SnakeCollisionEvent evt) {
    Object[] listeners = listenerList.getListenerList();
    for (int i = 0; i < listeners.length; i += 2) {
      if (listeners[i] == SnakeFieldEventListener.class) {
        ((SnakeFieldEventListener) listeners[i + 1]).snakeFieldCollision(evt);
      }
    }
  }

  private void fireFieldAdded(SnakeFieldEvent evt) {
    Object[] listeners = listenerList.getListenerList();
    for (int i = 0; i < listeners.length; i += 2) {
      if (listeners[i] == SnakeFieldEventListener.class) {
        ((SnakeFieldEventListener) listeners[i + 1]).snakeFieldAdded(evt);
      }
    }
  }

  private void fireFieldRemoved(SnakeFieldEvent evt) {
    Object[] listeners = listenerList.getListenerList();
    for (int i = 0; i < listeners.length; i += 2) {
      if (listeners[i] == SnakeFieldEventListener.class) {
        ((SnakeFieldEventListener) listeners[i + 1]).snakeFieldRemoved(evt);
      }
    }
  }

  /* (non-Javadoc)
   * @see Grid#getFreeField()
   */
  public Point getFreeField() {


    int x = (int) (size.width * Math.random());
    int y = (int) (size.height * Math.random());

    while (getField(new Point(x, y)) != null) {
      x = (int) (size.width * Math.random());
      y = (int) (size.height * Math.random());
    }

    return new Point(x, y);
  }

  /* (non-Javadoc)
   * @see Grid#isPossibleLocation(java.awt.Point)
   */
  public boolean isPossibleLocation(Point aPosition) {
    return (getField(aPosition) == null) || (getField(aPosition) instanceof Food);
  }

}