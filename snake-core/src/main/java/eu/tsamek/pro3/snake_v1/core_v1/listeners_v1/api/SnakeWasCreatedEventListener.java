package eu.tsamek.pro3.snake_v1.core_v1.listeners_v1.api;

import eu.tsamek.pro3.snake_v1.core_v1.events_v1.impl.SnakeWasCreatedEvent;

import java.util.EventListener;

/**
 * @author Tom� Samek
 */
public interface SnakeWasCreatedEventListener extends EventListener {

  /**
   * ud�lost vyvolan� vytvo�en�m hada
   *
   * @param evt SnakeWasCreatedEvent
   */
  void snakeWasCreated(SnakeWasCreatedEvent evt);


}
