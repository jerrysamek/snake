package eu.tsamek.pro3.snake_v1.core_v1.field_v1.impl;

import eu.tsamek.pro3.snake_v1.core_v1.api.Field;
import eu.tsamek.pro3.snake_v1.core_v1.impl.AbstractComponent;
import eu.tsamek.pro3.snake_v1.gui_v1.render_v1.api.SnakeRender;

import java.util.Map;

/**
 * @version 1.0
 * @created 24-VIII-2007 18:30:05
 * Abstraktn� pole ze kter�ho je tvo�en Grid
 */
public abstract class AbstractField extends AbstractComponent implements Field {

  protected SnakeRender render;

  /**
   * @param attributes Field.A_RENDER nastavuje render pro dan� pole
   */
  public AbstractField(Map<String, Object> attributes) {
    super(attributes);
    render = (SnakeRender) attributes.get(Field.A_RENDER);
  }

}