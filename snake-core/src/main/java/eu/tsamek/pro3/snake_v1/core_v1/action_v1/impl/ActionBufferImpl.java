package eu.tsamek.pro3.snake_v1.core_v1.action_v1.impl;

import eu.tsamek.pro3.snake_v1.core_v1.api.ActionBuffer;
import eu.tsamek.pro3.snake_v1.core_v1.impl.AbstractComponent;

import java.util.Map;
import java.util.Vector;

/**
 * @author Tom� Samek
 * <p>
 * Implementace interfacu ActionBuffer
 * @version 1.0
 * @created 24-VIII-2007 18:30:04
 */
public class ActionBufferImpl extends AbstractComponent implements ActionBuffer {

  private static final long serialVersionUID = 1151043047938696497L;
  Vector<String> buffer = new Vector<String>();

  /**
   * @param attributes null
   */
  public ActionBufferImpl(Map<String, Object> attributes) {
    super(attributes);
  }

  /* (non-Javadoc)
   * @see ActionBuffer#getAction()
   */
  public String getAction() {
    if (buffer == null || buffer.size() == 0) {
      return null;
    }

    String action = buffer.firstElement();
    buffer.remove(action);

    return action;
  }


  /* (non-Javadoc)
   * @see ActionBuffer#addAction(java.lang.String)
   */
  public void addAction(String aAction) {
    buffer.add(aAction);
  }

}