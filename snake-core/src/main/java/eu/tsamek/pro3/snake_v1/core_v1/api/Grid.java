package eu.tsamek.pro3.snake_v1.core_v1.api;

import eu.tsamek.pro3.snake_v1.core_v1.listeners_v1.api.SnakeFieldEventListener;

import java.awt.*;


/**
 * Obsahuje informace o um�st�n� a typu jednotliv�ch pol��ek na hrac�m poli
 *
 * @author Tom� Samek
 * @version 1.0
 * @created 24-VIII-2007 18:30:06
 */
public interface Grid {

  String A_DIMENSION = "Grid.Dimension";

  /**
   * p�id�v� pole do Gridu
   *
   * @param aPosition Point
   * @param aField    Field
   */
  void addField(Point aPosition, Field aField);

  /**
   * @return vrac� velikost pole
   */
  Dimension getSize();

  /**
   * odstran� pole z dan� pozice
   *
   * @param aPosition Point
   */
  void removeField(Point aPosition);

  /**
   * vr�t� pole z dan� pozice
   *
   * @param aPosition Point
   * @return Field
   */
  Field getField(Point aPosition);

  /**
   * zjist� jestli dan� pole p��stupn�
   *
   * @param aPosition
   * @return
   */
  boolean isPossibleLocation(Point aPosition);

  /**
   * @param listener SnakeFieldEventListener
   */
  void addSnakeFieldEventListener(SnakeFieldEventListener listener);

  /**
   * @param listener SnakeFieldEventListener
   */
  void removeSnakeFieldEventListener(SnakeFieldEventListener listener);

  /**
   * @return vrac� pole kter� je pr�zdn�
   */
  Point getFreeField();

  /**
   * vyvol�v� ud�lost kdy se zm�n� data na gridu
   */
  void fireDataChangedEvent();

}