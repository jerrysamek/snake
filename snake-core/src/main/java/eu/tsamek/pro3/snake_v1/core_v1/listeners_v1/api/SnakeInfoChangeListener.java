package eu.tsamek.pro3.snake_v1.core_v1.listeners_v1.api;

import eu.tsamek.pro3.snake_v1.core_v1.events_v1.impl.SnakeScoreEvent;

import java.util.EventListener;

/**
 * @author Tom� Samek
 */
public interface SnakeInfoChangeListener extends EventListener {

  /**
   * ud�lost vyvolan� zm�nou sk�re
   *
   * @param evt SnakeScoreEvent
   */
  void scoreChange(SnakeScoreEvent evt);

  /**
   * ud�lost vyvolan� zm�nou sk�re
   *
   * @param evt SnakeScoreEvent
   */
  void scoreBoardChange(SnakeScoreEvent evt);

}
