package eu.tsamek.pro3.snake_v1.core_v1.api;

import eu.tsamek.pro3.snake_v1.core_v1.listeners_v1.api.SnakeMoveEventListener;

import java.awt.*;

/**
 * @author Tom� Samek
 * Ukl�d� informace o hadoj
 * @version 1.0
 * @created 24-VIII-2007 18:30:07
 */
public interface Snake {
  String A_HEAD_POS = "attr.HeadPos";
  String A_SNAKE_DIRECTION = "attr.SnakeDir";
  String A_BODY_SIZE = "attr.SnakeSize";
  String A_CREATED_LISTENER = "attr.Listener";

  /**
   * @return vrac� pozici hlavy
   */
  Point getHead();

  /**
   * @return vrac� pozici ocasu
   */
  Point getTail();

  /**
   * vyvol� pohyb hada
   */
  void move();

  /**
   * zm�n� sm�r pohybu hada
   *
   * @param aDirection sm�r
   */
  void changeDirection(String aDirection);

  /**
   * @param listener SnakeMoveEventListener
   */
  void addSnakeMoveEventListener(SnakeMoveEventListener listener);

  /**
   * @param listener SnakeMoveEventListener
   */
  void removeSnakeMoveEventListener(SnakeMoveEventListener listener);

  /**
   * vyvol� r�st hada o jedno pole
   */
  void grow();

  /**
   * vyvol� zmen�en� hada o jedno pole
   */
  void reduction();

  /**
   * @return vr�t� velokost hada
   */
  int getSize();
}