package eu.tsamek.pro3.snake_v1.core_v1.impl;

import eu.tsamek.pro3.snake_v1.comm_v1.api.CommClient;
import eu.tsamek.pro3.snake_v1.comm_v1.api.ViperProtocol;
import eu.tsamek.pro3.snake_v1.comm_v1.events_1.impl.SnakeCommunicationEvent;
import eu.tsamek.pro3.snake_v1.comm_v1.listeners_v1.api.SnakeCommunicationEventListener;
import eu.tsamek.pro3.snake_v1.core_v1.api.ActionManager;
import eu.tsamek.pro3.snake_v1.core_v1.api.CollisionManager;
import eu.tsamek.pro3.snake_v1.core_v1.api.Field;
import eu.tsamek.pro3.snake_v1.core_v1.api.FieldManager;
import eu.tsamek.pro3.snake_v1.core_v1.api.Grid;
import eu.tsamek.pro3.snake_v1.core_v1.api.MoveManager;
import eu.tsamek.pro3.snake_v1.core_v1.api.Snake;
import eu.tsamek.pro3.snake_v1.core_v1.api.SnakeApplication;
import eu.tsamek.pro3.snake_v1.core_v1.events_v1.impl.SnakeCollisionEvent;
import eu.tsamek.pro3.snake_v1.core_v1.events_v1.impl.SnakeFieldEvent;
import eu.tsamek.pro3.snake_v1.core_v1.events_v1.impl.SnakeMoveEvent;
import eu.tsamek.pro3.snake_v1.core_v1.events_v1.impl.SnakeScoreEvent;
import eu.tsamek.pro3.snake_v1.core_v1.events_v1.impl.SnakeWasCreatedEvent;
import eu.tsamek.pro3.snake_v1.core_v1.field_v1.impl.Food;
import eu.tsamek.pro3.snake_v1.core_v1.listeners_v1.api.SnakeFieldEventListener;
import eu.tsamek.pro3.snake_v1.core_v1.listeners_v1.api.SnakeInfoChangeListener;
import eu.tsamek.pro3.snake_v1.core_v1.listeners_v1.api.SnakeMoveEventListener;
import eu.tsamek.pro3.snake_v1.core_v1.listeners_v1.api.SnakeWasCreatedEventListener;
import eu.tsamek.pro3.snake_v1.gui_v1.impl.AppGUI;
import eu.tsamek.pro3.snake_v1.gui_v1.render_v1.impl.AlienSnakePartRender;
import eu.tsamek.pro3.snake_v1.gui_v1.render_v1.impl.FoodRender;
import eu.tsamek.pro3.snake_v1.gui_v1.render_v1.impl.ObstructionRender;
import eu.tsamek.pro3.snake_v1.gui_v1.render_v1.impl.SnakePartRender;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

/**
 * T��da staraj�c� se o hladk� b�h hada
 *
 * @author Tom� Samek
 * @version 1.0
 * @created 24-VIII-2007 18:30:07
 */
public class SnakeRunner implements SnakeApplication {

  private static boolean noGui = false;
  boolean scoreLock = false;
  int sleepTime = SNAKE_WAIT_TIME;
  private Grid grid;
  private Snake snake;
  private ActionManager actionManager;
  private FieldManager fieldManager;
  private CommClient client = null;
  private Point startPosition = DEFAULT_START_POINT;
  private String startDirection = DEFAULT_DIRECTION;
  private String host = "localhost";
  private int port = 6666;
  private int score = 0;
  private boolean death = false;
  private boolean serverStop = false;
  private boolean multi = true;
  private Point[] food = new Point[20];
  private Map<String, String> scoreMap = new HashMap<String, String>();
  private boolean dataLock = false;
  private Map<String, Object> dataForSend = new HashMap<String, Object>();
  /**
   * slou�� k porovn�v�n� vzd�lenost� hada od j�dla
   */
  private Comparator<Point> comp = new Comparator<Point>() {

    public int compare(Point p1, Point p2) {

      if (p1 == null && p2 == null) {
        return 0;
      }
      if (p1 != null && p2 == null) {
        return -1;
      }
      if (p1 == null && p2 != null) {
        return 1;
      }
      if (snake.getHead().distance(p1) > snake.getHead().distance(p2)) {
        return 1;
      } else if (snake.getHead().distance(p1) < snake.getHead().distance(p2)) {
        return -1;
      }

      return 0;
    }

  };
  private javax.swing.event.EventListenerList listenerList =
      new javax.swing.event.EventListenerList();

  /**
   * konstruktor vytv��ej�c� instanci aplikace hada
   *
   * @param manager umo��uje nastavit t��du upravuj�c� chov�n� hada
   */
  public SnakeRunner(ActionManager manager) {
    appInit(manager);
  }

  /**
   * konstruktor vytv��ej�c� instanci aplikace hada
   */
  public SnakeRunner() {
    appInit();
  }

  /**
   * konstruktor vytv��ej�c� instanci aplikace hada
   *
   * @param host host na kter�m se m� aplikace p�ipojit k serveru
   * @param port port na kter�m se m� aplikace p�ipojit k serveru
   */
  public SnakeRunner(String host, int port) {
    this.host = host;
    this.port = port;
    appInit();
  }

  /**
   * konstruktor vytv��ej�c� instanci aplikace hada
   *
   * @param host    host na kter�m se m� aplikace p�ipojit k serveru
   * @param port    port na kter�m se m� aplikace p�ipojit k serveru
   * @param manager umo��uje nastavit t��du upravuj�c� chov�n� hada
   */
  public SnakeRunner(String host, int port, ActionManager manager) {
    this.host = host;
    this.port = port;
    appInit(manager);
  }

  /**
   * @param args 1 - host, 2 - port, 3 - AI
   */
  public static void init(String[] args, boolean noG) {
    noGui = noG;
    if (args.length < 3 && args.length > 0) {
      System.out.println("Required arguments: <host> <port> AI<true/false>");
      return;
    }
    if (args.length == 3) {

      String host = args[0];
      int port = Integer.parseInt(args[1]);

      if (!Boolean.parseBoolean(args[2])) {
        new SnakeRunner(
            host,
            port,
            (ActionManager) ComponentFactory.createComponent(ComponentFactory.COM_ACTIONMANAGER));
      } else {
        new SnakeRunner(host, port);
      }
    } else {
      new SnakeRunner();
    }
  }

  /**
   * p�id�v� nov� data do mapy kter� se n�sledn� ode�le na served
   *
   * @param key   kl�� - String
   * @param value hodnota - Object
   */
  private synchronized void addNewData(String key, Object value) {

    while (dataLock) {
      try {
        Thread.sleep(1);
      } catch (InterruptedException e) {}
    }

    dataLock = true;

    dataForSend.put(key, value);

    dataLock = false;
  }

  private synchronized void addNewScore(String key, String value) {

    while (scoreLock) {
      try {
        Thread.sleep(1);
      } catch (InterruptedException e) {}
    }

    scoreLock = true;
    scoreMap.put(key, value);
    fireScoreBoardChange(new SnakeScoreEvent(this, scoreMap));
    scoreLock = false;
  }

  /**
   * inicializuje pole
   *
   * @param size velikost pole
   */
  private void initGrid(Dimension size) {

    Map<String, Object> param = new HashMap<String, Object>();

    param.put(Grid.A_DIMENSION, size);

    grid = (Grid) ComponentFactory.createComponent(ComponentFactory.COM_GRID, param);

    for (int i = 0; i < size.height; i++) {

      param.put(Field.A_RENDER, new ObstructionRender(FIELD_DIMENSION));

      grid.addField(new Point(0, i), fieldManager.createField(FieldManager.FIELD_OBSTRUCTION, param));
      grid.addField(new Point(size.width - 1, i), fieldManager.createField(FieldManager.FIELD_OBSTRUCTION, param));

    }
    for (int i = 0; i < size.width; i++) {

      grid.addField(new Point(i, 0), fieldManager.createField(FieldManager.FIELD_OBSTRUCTION, param));
      grid.addField(new Point(i, size.height - 1), fieldManager.createField(FieldManager.FIELD_OBSTRUCTION, param));

    }

  }

  /**
   * inicializace samotn�ho hada
   *
   * @param aHead      hlava hada
   * @param aBodySize  velikost hada
   * @param aDirection sm�r hada
   */
  private void initSnake(Point aHead, int aBodySize, String aDirection) {

    Map<String, Object> param = new HashMap<String, Object>();

    param.put(Snake.A_HEAD_POS, aHead);

    param.put(Snake.A_CREATED_LISTENER, new SnakeWasCreatedEventListener() {

      public void snakeWasCreated(SnakeWasCreatedEvent evt) {

        Vector<Point> body = evt.getBody();

        Map<String, Object> param = new HashMap<String, Object>();
        param.put(Field.A_RENDER, new SnakePartRender(FIELD_DIMENSION));

        for (Iterator iter = body.iterator(); iter.hasNext(); ) {
          Point element = (Point) iter.next();
          grid.addField(element, fieldManager.createField(FieldManager.FIELD_SNAKEPART, param));

        }
        addNewData(A_COMM_SNAKE, body);
      }

    });

    param.put(Snake.A_BODY_SIZE, aBodySize);
    param.put(Snake.A_SNAKE_DIRECTION, aDirection);

    snake = (Snake) ComponentFactory.createComponent(ComponentFactory.COM_SNAKE, param);

  }

  /**
   * instaluje listenery na klienta kter� ud�vaj� jeho stav a pomoc� ni� p�ed�v� aplikaci p�ijat� data
   */
  private void installCommListeners() {

    client.addCommunicationEventListener(new SnakeCommunicationEventListener() {

      @SuppressWarnings("unchecked")
      public void dataReceived(SnakeCommunicationEvent evt) {

        if (evt.getData().containsKey(A_COMM_SNAKE)) {
          Vector<Point> body = (Vector<Point>) evt.getData().get(A_COMM_SNAKE);

          for (Iterator iter = body.iterator(); iter.hasNext(); ) {
            Point element = (Point) iter.next();
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(Field.A_RENDER, new AlienSnakePartRender(FIELD_DIMENSION));
            grid.addField(element, fieldManager.createField(FieldManager.FIELD_SNAKEPART, param));

          }
        }
        if (evt.getData().containsKey(A_COMM_SCORE) && evt.getData().containsKey(A_COMM_CLIENTNAME)) {

          addNewScore(evt.getData().get(A_COMM_CLIENTNAME).toString(), evt.getData().get(A_COMM_SCORE).toString());

        }
        if (evt.getData().containsKey(ViperProtocol.DATA_START)) {
          new Mover().start();
        }

        if (evt.getData().containsKey(A_COMM_FOOD)) {

          createFood((Point) evt.getData().get(A_COMM_FOOD));

        }
        if (evt.getData().containsKey(A_COMM_ADD)) {
          Map<String, Object> param = new HashMap<String, Object>();
          param.put(Field.A_RENDER, new AlienSnakePartRender(FIELD_DIMENSION));

          grid.addField(
              (Point) evt.getData().get(A_COMM_ADD),
              fieldManager.createField(FieldManager.FIELD_SNAKEPART, param));

        }
        if (evt.getData().containsKey(A_COMM_REMOVE)) {
          grid.removeField((Point) evt.getData().get(A_COMM_REMOVE));
        }

      }

      public void serverConnected(SnakeCommunicationEvent evt) {

        if (evt.getData().containsKey(ViperProtocol.DATA_STARTPOSITION)) {
          startPosition = (Point) evt.getData().get(ViperProtocol.DATA_STARTPOSITION);
        }
        if (evt.getData().containsKey(ViperProtocol.DATA_STARTDIRECTION)) {
          startDirection = (String) evt.getData().get(ViperProtocol.DATA_STARTDIRECTION);
        }

      }

      public void serverDisconnected() {
        serverStop = true;
        try {
          client.disconnect();
        } catch (IOException e) {
          e.printStackTrace();
        }
        client = null;
      }

    });

  }

  /**
   * instaluje standartn� listenery k d�le�it�m objekt�m
   */
  private void installListeners() {

    snake.addSnakeMoveEventListener(new SnakeMoveEventListener() {

      public void snakeMoved(SnakeMoveEvent evt) {

        Map<String, Object> param = new HashMap<String, Object>();
        param.put(Field.A_RENDER, new SnakePartRender(FIELD_DIMENSION));

        grid.addField(evt.getNewPosition(), fieldManager.createField(FieldManager.FIELD_SNAKEPART, param));
        grid.removeField(evt.getOldPosition());

        addNewData(A_COMM_REMOVE, evt.getOldPosition());
        addNewData(A_COMM_ADD, evt.getNewPosition());

      }

    });

    grid.addSnakeFieldEventListener(new SnakeFieldEventListener() {

      public void snakeFieldAdded(SnakeFieldEvent evt) {

      }

      public void snakeFieldRemoved(SnakeFieldEvent evt) {

      }

      public void snakeFieldCollision(SnakeCollisionEvent evt) {
        String collision = evt.getCollision();

        if (collision != null && (collision.equals(CollisionManager.COLLISION_OBSTRUCTION)
            || (collision.equals(CollisionManager.COLLISION_SNAKE_PART)))) {

          if (snake.getHead().x == evt.getPosition().x && snake.getHead().y == evt.getPosition().y) {
            death = true;
          }
        } else if (collision != null && collision.equals(CollisionManager.COLLISION_SNAKE_FOOD)) {

          if (snake.getHead().x == evt.getPosition().x && snake.getHead().y == evt.getPosition().y) {

            snake.grow();

            fireScoreChange(new SnakeScoreEvent(this, ++score));

            addNewData(A_COMM_SCORE, score);

            createFood();
          }
          for (int i = 0; i < food.length; i++) {
            if (food[i] != null && !(grid.getField(food[i]) instanceof Food)) {
              food[i] = null;
            }
          }

          Arrays.sort(food, comp);
        }
      }

      public void snakeDataChanged() {

      }
    });

  }

  /**
   * vytv��� novou polo�ku j�dla na poli na specifick� lokaci
   *
   * @param p Point pozice j�dla
   */
  public synchronized void createFood(Point p) {

    Map<String, Object> param = new HashMap<String, Object>();
    param.put(Field.A_RENDER, new FoodRender(FIELD_DIMENSION));
    food[19] = p;

    Arrays.sort(food, comp);

    grid.addField(p, fieldManager.createField(FieldManager.FIELD_SNAKEFOOD, param));

  }

  /**
   * vytv��� novou polo�ku j�dla na poli na n�hodn� voln� pozici
   */
  public synchronized void createFood() {

    Point food = grid.getFreeField();
    createFood(food);
    addNewData(A_COMM_FOOD, food);

  }

  /**
   * Vytv��� komunika�n�ho klienta pro komunikaci se serverem
   *
   * @param host host na kter�m se m� aplikace p�ipojit k serveru
   * @param port port na kter�m se m� aplikace p�ipojit k serveru
   * @return CommClient
   */
  private CommClient createConnectionManager(String host, int port) {

    Map<String, Object> params = new HashMap<String, Object>();
    params.put(CommClient.A_HOST, host);
    params.put(CommClient.A_PORT, port);

    return (CommClient) ComponentFactory.createComponent(ComponentFactory.COM_COMMCLIENT, params);

  }

  /**
   * inicializuje JFrame obsahuj�c� hada
   *
   * @return AppGUI
   */
  private AppGUI initFrame() {
    AppGUI frame = new AppGUI(this, grid, FIELD_DIMENSION);
    frame.addWindowListener(new WindowAdapter() {

      public void windowClosing(WindowEvent e) {

        if (client != null) {
          try {
            client.sendServerDisconnectRequest();
            client.disconnect();
          } catch (IOException e1) {
            e1.printStackTrace();
          }
        }
      }
    });
    return frame;

  }

  /**
   * inicializuje aplikaci
   */
  private void appInit() {
    appInit(new ActionManager() {
              double dist;

              private boolean isPossibleLocation(Point aPosition) {
                return grid.isPossibleLocation(aPosition);
              }

              private boolean isLowerDistance(Point direction) {

                Point newPosition = (Point) snake.getHead().clone();

                newPosition.translate(direction.x, direction.y);

                Arrays.sort(food, comp);

                if (dist > newPosition.distance(food[0]) && isPossibleLocation(newPosition)) {
                  dist = newPosition.distance(food[0]);
                  return true;
                }
                return false;

              }

              public String getAction() {
                dist = 50000;
                String action = null;

                if (isLowerDistance(MoveManager.MOVE_DOWN)) {
                  action = ACT_KEY_DOWN;
                }

                if (isLowerDistance(MoveManager.MOVE_UP)) {
                  action = ACT_KEY_UP;
                }

                if (isLowerDistance(MoveManager.MOVE_LEFT)) {
                  action = ACT_KEY_LEFT;
                }

                if (isLowerDistance(MoveManager.MOVE_RIGHT)) {
                  action = ACT_KEY_RIGHT;
                }

                return action;
              }

              public void keyPressed(KeyEvent e) {

              }

              public void keyReleased(KeyEvent e) {
              }

              public void keyTyped(KeyEvent e) {
              }

            }
           );
  }

  /**
   * inicializuje aplikaci
   *
   * @param manager umo��uje nastavit t��du upravuj�c� chov�n� hada
   */
  private void appInit(ActionManager manager) {

    actionManager = manager;

    fieldManager = (FieldManager) ComponentFactory.createComponent(ComponentFactory.COM_FIELDMANAGER);

    initGrid(GRID_DIMENSION);
    if (!noGui) {
      initFrame().addKeyListener(actionManager);
    }

    if (multi) {
      client = createConnectionManager(host, port);
      if (client != null) {
        installCommListeners();
      } else {
        if (noGui) {
          System.exit(0);
        }
      }
    } else {
      new Mover().start();
    }
  }

  /**
   * t��da instaluje listenery k aplikaci
   *
   * @param listener SnakeInfoChangeListener
   */
  public void addSnakeInfoChangeListener(SnakeInfoChangeListener listener) {
    listenerList.add(SnakeInfoChangeListener.class, listener);
  }

  /**
   * t��da odinstaluje listenery k aplikaci
   *
   * @param listener SnakeInfoChangeListener
   */
  public void removeSnakeInfoChangeListener(SnakeInfoChangeListener listener) {
    listenerList.remove(SnakeInfoChangeListener.class, listener);
  }

  /**
   * vyvol� ud�lost oznamuj�c� zm�nu sk�re
   *
   * @param evt SnakeScoreEvent
   */
  protected void fireScoreChange(SnakeScoreEvent evt) {
    Object[] listeners = listenerList.getListenerList();
    for (int i = 0; i < listeners.length; i += 2) {
      if (listeners[i] == SnakeInfoChangeListener.class) {
        ((SnakeInfoChangeListener) listeners[i + 1]).scoreChange(evt);
      }
    }
  }

  /**
   * vyvol� ud�lost oznamuj�c� zm�nu sk�re u ostatn�ch had�
   *
   * @param evt SnakeScoreEvent
   */
  protected void fireScoreBoardChange(SnakeScoreEvent evt) {
    Object[] listeners = listenerList.getListenerList();
    for (int i = 0; i < listeners.length; i += 2) {
      if (listeners[i] == SnakeInfoChangeListener.class) {
        ((SnakeInfoChangeListener) listeners[i + 1]).scoreBoardChange(evt);
      }
    }
  }

  /**
   * @author tom
   * T��da staraj�c� se o pohyb hada
   */
  private class Mover extends Thread {

    @Override
    public void run() {

      initSnake(startPosition, 5, startDirection);
      installListeners();
      createFood();

      grid.fireDataChangedEvent();
      String clientname = "AI".concat(String.valueOf(Math.round(9999 * Math.random() + 1)));

      if (SNAKE_WAIT_TIME - score * 5 > 50) {
        sleepTime = SNAKE_WAIT_TIME - score * 5;
      }
      while (!death && !serverStop) {

        try {
          Thread.sleep(sleepTime);
        } catch (InterruptedException e1) {
          e1.printStackTrace();
        }

        String action = actionManager.getAction();

        if (action == null) {

        } else if (action.equals(ActionManager.ACT_KEY_DOWN)) {
          snake.changeDirection(MoveManager.DIR_DOWN);
        } else if (action.equals(ActionManager.ACT_KEY_UP)) {
          snake.changeDirection(MoveManager.DIR_UP);
        } else if (action.equals(ActionManager.ACT_KEY_LEFT)) {
          snake.changeDirection(MoveManager.DIR_LEFT);
        } else if (action.equals(ActionManager.ACT_KEY_RIGHT)) {
          snake.changeDirection(MoveManager.DIR_RIGHT);
        }

        snake.move();

        if (client != null) {
          client.createPacket(dataForSend);
          try {
            dataLock = true;
            client.sendPacket();
            dataLock = false;
          } catch (IOException e) {
            System.out.println("Nem��u odeslat packet");
            client = null;
          }
        }
        dataForSend = new HashMap<String, Object>();
        addNewData(A_COMM_CLIENTNAME, clientname);
      }
      if (!noGui) {
        JOptionPane.showMessageDialog(null, "U R Death");
      } else {
        if (client != null) {
          try {
            client.disconnect();
          } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
          }
        }
      }

    }

  }

}