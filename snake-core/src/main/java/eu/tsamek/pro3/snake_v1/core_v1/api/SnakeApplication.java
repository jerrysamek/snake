package eu.tsamek.pro3.snake_v1.core_v1.api;

import eu.tsamek.pro3.snake_v1.core_v1.listeners_v1.api.SnakeInfoChangeListener;

import java.awt.*;

/**
 * @author Tom� Samek
 * definuje standartn� aplikaci pro hada s defaultn�mi konstantami
 */
public interface SnakeApplication {
  String A_COMM_CLIENTNAME = "Data.ClientName";
  String A_COMM_SCORE = "Data.Score";
  String A_COMM_REMOVE = "Data.Remove";
  String A_COMM_ADD = "Data.Add";
  String A_COMM_FOOD = "Data.Food";
  String A_COMM_SNAKE = "Data.Snake";

  String DEFAULT_DIRECTION = MoveManager.DIR_RIGHT;
  Point DEFAULT_START_POINT = new Point(7, 5);
  Dimension FIELD_DIMENSION = new Dimension(12, 12);
  Dimension GRID_DIMENSION = new Dimension(40, 30);

  int SNAKE_WAIT_TIME = 250;

  void addSnakeInfoChangeListener(SnakeInfoChangeListener listener);
}
