package eu.tsamek.pro3.snake_v1.core_v1.field_v1.impl;

import eu.tsamek.pro3.snake_v1.core_v1.api.Component;
import eu.tsamek.pro3.snake_v1.core_v1.api.Field;
import eu.tsamek.pro3.snake_v1.core_v1.api.FieldManager;
import eu.tsamek.pro3.snake_v1.core_v1.impl.AbstractComponent;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

/**
 * @author Tom� Samek
 * <p>
 * T��da implementuje FieldManager a slou�� k vytv��en� jednotliv�ch pol�
 * @version 1.0
 * @created 24-VIII-2007 18:30:06
 */
public class FieldManagerImpl extends AbstractComponent implements FieldManager {

  private static final long serialVersionUID = -1484531932288238609L;

  /**
   * {@inheritDoc}
   */
  public FieldManagerImpl(Map<String, Object> attributes) {
    super(attributes);
  }

  /* (non-Javadoc)
   * @see FieldManager#createField(java.lang.String, java.util.Map)
   */
  @SuppressWarnings("unchecked")
  public Field createField(String aName, Map<String, Object> aParams) {

    try {
      java.lang.reflect.Field[] fields = this.getClass().getFields();
      String className = null;
      int i = 0;
      while (i < fields.length && className == null) {
        if (fields[i].get(fields[i].getName()).equals(aName)) {

          java.lang.reflect.Field field = this.getClass().getField(fields[i].getName().concat(CONST_POSTFIX));
          className = field.get(field.getName()).toString();
        }
        i++;
      }
      if (className == null) {
        throw new NoSuchFieldException();
      }
      Class<Component> clazz = (Class<Component>) Class.forName(CLASS_PREFIX.concat(className));
      Constructor<Component> constructor = clazz.getConstructor(Map.class);

      return (Field) constructor.newInstance(aParams);

    } catch (SecurityException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (ClassNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (NoSuchMethodException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IllegalArgumentException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (InstantiationException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (InvocationTargetException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (NoSuchFieldException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return null;


  }

  /* (non-Javadoc)
   * @see FieldManager#createField(java.lang.String)
   */
  public Field createField(String aName) {
    return createField(aName, null);
  }

}