package eu.tsamek.pro3.snake_v1.core_v1.api;

import java.awt.event.KeyListener;

/**
 * @author Tom� Samek
 * @version 1.0
 * @created 24-VIII-2007 18:30:04
 */
public interface ActionManager extends KeyListener {

  String ACT_KEY_DOWN = "Key.Down";
  String ACT_KEY_LEFT = "Key.Left";
  String ACT_KEY_RIGHT = "Key.Right";
  String ACT_KEY_UP = "Key.Up";

  /**
   * @return vrac� String akce kter� byla vyvol�na implementac� ActionManager
   */
  String getAction();


}