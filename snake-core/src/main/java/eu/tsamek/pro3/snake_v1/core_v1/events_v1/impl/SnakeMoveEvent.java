package eu.tsamek.pro3.snake_v1.core_v1.events_v1.impl;

import java.awt.*;
import java.util.EventObject;

/**
 * @author Tom� Samek
 * Akce vyvolan� pohybem hada
 */
public class SnakeMoveEvent extends EventObject {

  private static final long serialVersionUID = -1428137080686881603L;
  Point newPosition;
  Point oldPosition;

  public SnakeMoveEvent(Object source, Point newPosition, Point oldPosition) {
    super(source);
    this.newPosition = newPosition;
    this.oldPosition = oldPosition;
  }

  /**
   * @return vrac� novou pozici hlavy
   */
  public Point getNewPosition() {
    return newPosition;
  }

  /**
   * @return vrac� starou pozici ocasu
   */
  public Point getOldPosition() {
    return oldPosition;
  }

}
