package eu.tsamek.pro3.snake_v1.core_v1.api;

/**
 * @author Tom� Samek
 * @version 1.0
 * @created 24-VIII-2007 18:30:03
 */
public interface ActionBuffer {

  /**
   * @return vrac� string reprezentuj�c� prvn� akci v bufferu
   */
  String getAction();

  /**
   * vkl�d� novou akci do bufferu
   *
   * @param aAction String
   */
  void addAction(String aAction);

}