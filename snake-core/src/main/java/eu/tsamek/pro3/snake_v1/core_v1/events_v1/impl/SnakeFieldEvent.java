package eu.tsamek.pro3.snake_v1.core_v1.events_v1.impl;

import java.awt.*;
import java.util.EventObject;

/**
 * @author Tom� Samek
 * ud�lost vyvolan� zm�nou pole
 */
public class SnakeFieldEvent extends EventObject {

  private static final long serialVersionUID = -9156566695474271365L;
  private Point part;

  public SnakeFieldEvent(Object source, Point part) {
    super(source);
    this.part = part;
  }

  /**
   * @return vrac� pole kter� se zm�nilo
   */
  public Point getPart() {
    return part;
  }

}
