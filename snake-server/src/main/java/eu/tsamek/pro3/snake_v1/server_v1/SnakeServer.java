package eu.tsamek.pro3.snake_v1.server_v1;

import eu.tsamek.pro3.snake_v1.comm_v1.api.ViperProtocol;
import eu.tsamek.pro3.snake_v1.comm_v1.impl.Packet;
import eu.tsamek.pro3.snake_v1.core_v1.api.MoveManager;

import java.awt.*;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

public class SnakeServer implements ViperProtocol {

  public final int MAX_CLIENTS = 2;
  private int numberOfCli = MAX_CLIENTS;
  private int port = 6666;

  public SnakeServer() {
    initServer();
  }

  public SnakeServer(int numberOfCli, int port) {
    this.numberOfCli = numberOfCli;
    initServer();
  }

  public static void main(String[] args) {
    if (args.length == 2) {
      new SnakeServer(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
    } else {
      new SnakeServer();
    }
  }

  /**
   *
   */
  private void initServer() {
    Vector<Point> startPositions = new Vector<Point>();
    Vector<String> startDirections = new Vector<String>();

    try {
      ServerSocket server = new ServerSocket(port);
      server.setPerformancePreferences(1, 1, 1);
      server.setReceiveBufferSize(655360);
      System.out.println("Server started ...");

      Vector<SocketHandler> clients = new Vector<SocketHandler>();

      startPositions.add(new Point(7, 5));
      startPositions.add(new Point(7, 25));
      startPositions.add(new Point(33, 5));
      startPositions.add(new Point(33, 25));

      startDirections.add(MoveManager.DIR_RIGHT);
      startDirections.add(MoveManager.DIR_LEFT);
      startDirections.add(MoveManager.DIR_RIGHT);
      startDirections.add(MoveManager.DIR_LEFT);

      for (int i = 0; i < numberOfCli; i++) {

        System.out.println("Waiting for " + String.valueOf(MAX_CLIENTS - i) + " clients ... ");
        Socket client = server.accept();

        client.setSendBufferSize(163840);
        client.setReceiveBufferSize(163840);
        client.setKeepAlive(true);
        System.out.println("Client number " + String.valueOf(i + 1) + " was accepted ... ");
        SocketHandler handler = new SocketHandler(client, i);

        handler.setStartDirection(startDirections.get(i));
        handler.setStartPosition(startPositions.get(i));

        clients.add(handler);
      }
      for (Iterator iter = clients.iterator(); iter.hasNext(); ) {
        SocketHandler element = (SocketHandler) iter.next();
        element.setClients(clients);
        element.start();
      }

    } catch (IOException e) {

      e.printStackTrace();
    }
  }

  private class SocketHandler extends Thread {

    private Socket client;
    private Vector<SocketHandler> clients = new Vector<SocketHandler>();
    private int clientID;
    private Point startPosition;
    private String startDirection;
    private ObjectOutputStream oos;
    private ObjectInputStream ois;

    public SocketHandler(Socket client, int id) {
      this.client = client;
      this.clientID = id;

    }

    public synchronized void setStartDirection(String startDirection) {
      this.startDirection = startDirection;
    }

    public synchronized void setStartPosition(Point startPosition) {
      this.startPosition = startPosition;
    }

    public synchronized void sendPacket(Packet p) {

      try {
        if (oos != null && !client.isClosed() && !client.isOutputShutdown()) {
          oos.writeObject(p);
        }
      } catch (IOException e) {
        try {
          oos.close();
        } catch (IOException e1) {}
        oos = null;
        e.printStackTrace();
      }
    }

    @Override
    public void run() {

      try {
        oos = new ObjectOutputStream(client.getOutputStream());
        ois = new ObjectInputStream(client.getInputStream());

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(DATA_IDENTITY, clientID);
        params.put(DATA_STARTPOSITION, startPosition);
        params.put(DATA_STARTDIRECTION, startDirection);

        sendPacket(new Packet(params, SERVERSTATUS_IDENTIFICATION));

        params = new HashMap<String, Object>();
        params.put(DATA_START, null);
        sendPacket(new Packet(params, SERVERSTATUS_DATA));

        Packet input;

        while (oos != null && ois != null && !client.isClosed() && !client.isInputShutdown() && (input = (Packet) ois.readObject()) != null
            && (input.getStatus() != CLIENTSTATUS_DISCONNECT)) {

          for (int i = 0; i < clients.size(); i++) {
            SocketHandler element = clients.get(i);

            if (element.getClientId() != clientID
                && input.getStatus() == SERVERSTATUS_DATA
                && input.getData() != null) {

              Packet packet = new Packet(
                  input.getData(),
                  SERVERSTATUS_DATA);
              if (!element.getClient().isClosed()) {
                element.sendPacket(packet);
              }
            }
          }
          if (input.getStatus() == SERVERSTATUS_DISCONNECT) {
            for (int i = 0; i < clients.size(); i++) {
              SocketHandler element = clients.get(i);

              if (element.getClientId() != clientID) {

                element.sendPacket(new Packet(null, SERVERSTATUS_DISCONNECT));
              }
            }
          }

        }


        Thread.sleep(1000);

        if (ois != null) {
          ois.close();
        }
        if (oos != null) {
          oos.close();
        }

        client.close();
      } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (ClassNotFoundException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (InterruptedException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }

      System.out.println("Client is disconnected!");

    }

    public void setClients(Vector<SocketHandler> clients) {
      this.clients = clients;
    }

    public synchronized int getClientId() {
      return clientID;
    }

    public synchronized Socket getClient() {
      return client;
    }

  }
}
